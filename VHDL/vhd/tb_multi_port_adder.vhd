-------------------------------------------------------------------------------
-- Title      : E04 - test bench for multi port adder
-- Project    : Logic Synthesis Course - TIE-50206
-------------------------------------------------------------------------------
-- File       : tb_multi_port_adder.vhd
-- Author     : Mohamad Ibrahim Shariat Nasseri - 256303
-- Company    : 
-- Created    : 2016-12-04
-- Last update: 2016-12-04
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: it is the testbench for testing the functionality of the
--              multi port adder from E04
-------------------------------------------------------------------------------
-- Copyright (c) 2016 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-12-04  1.0      Mo      Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;


entity tb_multi_port_adder is
  generic (
    operand_width_g : integer := 3      --width of each operands
    );
end tb_multi_port_adder;

architecture testbench of tb_multi_port_adder is

  -- type definition for saving each line values.
  type int_array is array (integer range 0 to 3) of integer range -4 to 2;

  -- constants declarations
  constant period_c          : time    := 10 ns;
  constant num_of_operands_c : integer := 4;
  constant DUV_delay_c       : integer := 2;

  -- signals declarations
  signal clk            : std_logic := '0';
  signal rst_n          : std_logic := '0';
  signal operands_r     : std_logic_vector ((operand_width_g*num_of_operands_c)-1 downto 0);
  signal sum            : std_logic_vector (operand_width_g-1 downto 0);
  signal output_valid_r : std_logic_vector (DUV_delay_c downto 0);  --:= "001";

  -- input/output file declarations
  file input_f       : text open read_mode is "input.txt";
  file ref_results_f : text open read_mode is "ref_results.txt";
  file output_f      : text open write_mode is "output.txt";

  -- Signal for checking valu_v value in ModelSim
  signal invalue_sig : int_array := (0, 0, 0, 0);

  --Component Declaration
  component multi_port_adder is
    generic (
      operand_width_g   : integer;      --Width of each input
      num_of_operands_g : integer       --Number of inputs
      );

    port(
      clk   : in std_logic;
      rst_n : in std_logic;

      --input with the width equal to the total width of all operands together
      operands_in : in std_logic_vector ((operand_width_g*num_of_operands_g)-1 downto 0);

      --Result of addidtion
      sum_out : out std_logic_vector (operand_width_g-1 downto 0)
      );
  end component;

begin

  -- Clock and reset
  clk   <= not(clk) after period_c/2;
  rst_n <= '1'      after 4*period_c;

  --Mapping to the testbench
  adder_map : multi_port_adder
    generic map (operand_width_g   => operand_width_g,
                 num_of_operands_g => num_of_operands_c
                 )
    port map (clk         => clk,
              rst_n       => rst_n,
              operands_in => operands_r,
              sum_out     => sum
              );

  -- reading inputs and feeding the component
  input_reader : process (clk, rst_n)

    variable line_v  : line;
    variable value_v : int_array;       --input values

  begin

    -- reseting
    if rst_n = '0' then

      operands_r     <= (others => '0');
      output_valid_r <= "001";

    elsif clk'event and clk = '1' then

      -- making the delay. it uses "rotate left" function
      output_valid_r <= std_logic_vector (unsigned (output_valid_r) rol 1);

      --Reading inputs
      if output_valid_r (DUV_delay_c) = '1' then
        if not endfile(input_f) then
          readline (input_f, line_v);
          read_inputs : for i in 0 to 3 loop
            read (line_v, value_v(i));
            operands_r (operand_width_g*(i+1)-1 downto operand_width_g*i) <= std_logic_vector (to_signed(value_v (i), operand_width_g));
          end loop;

                                        -- To check the value_v in ModelSim
          invalue_sig <= value_v;
        end if;
      end if;
    end if;
  end process;

-- Comapring the result with the reference values

  checker : process (clk, rst_n)

    variable line_v     : line;
    variable outvalue_v : integer;      --output values
    variable refvalue_v : integer;      --reference values

  begin
    if rst_n = '0' then

      outvalue_v := 0;

    elsif clk'event and clk = '1' then

      if output_valid_r (DUV_delay_c) = '1' then
        if not endfile(ref_results_f) then
          readline (ref_results_f, line_v);
          read (line_v, refvalue_v);

                                        --convert addidtion result and write to the output file
          write (line_v, outvalue_v);
          outvalue_v := to_integer (signed (sum));
          writeline (output_f, line_v);

                                        --when there's an invalid result
          assert outvalue_v = to_integer (signed (sum)) report "Wrong value calculated. simulation aborted" severity failure;
        else

                                        -- all results were checked with ref values successfully
          assert false report "Simulation done!" severity failure;
        end if;
      end if;
    end if;
  end process;
end;
