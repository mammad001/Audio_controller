-------------------------------------------------------------------------------
-- Title      : E03 - Generic Adder
-- Project    : Logic Synthesis Course (TIE-50206)
-------------------------------------------------------------------------------
-- File       : adder.vhd
-- Author     : Mohamad Ibrahim Shariat Nasseri (256303)
-- Company    : 
-- Created    : 2016-11-19
-- Last update: 2016-11-20
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: This code initially is written for a 3-bit ripple-carry adder.
-- Can be expanded to any desired width
-------------------------------------------------------------------------------
-- Copyright (c) 2016 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-11-19  1.0      Mo      Created
-------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity adder is
  generic (
    operand_width_g : integer           -- width of inputs
    );

  port(
    clk     : in  std_logic;
    rst_n   : in  std_logic;
    a_in    : in  std_logic_vector (operand_width_g-1 downto 0);
    b_in    : in  std_logic_vector (operand_width_g-1 downto 0);
    sum_out : out std_logic_vector (operand_width_g downto 0)
    );
end adder;

architecture rtl of adder is

  -- Internal signal for doing signed addition
  signal result : signed (operand_width_g downto 0);

begin
  process (clk, rst_n)
  begin
    -- Asynch reset
    if rst_n = '0' then
      result <= (others => '0');

    -- Synchronous adder
	-- inputs are resized and converted to signed type to do the addition	
    elsif (clk'event and clk = '1') then
      result <= resize (signed (a_in), operand_width_g+1) + resize (signed (b_in), operand_width_g+1);
      
    end if;
  end process;

  -- moving the addition result to the output
  sum_out <= std_logic_vector (result);
  
end;
