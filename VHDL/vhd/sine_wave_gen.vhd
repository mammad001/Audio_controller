-------------------------------------------------------------------------------
-- Title      : E06 - Triangular waveform generator
-- Project    : TIE-50206 - Logic Synthesis
-------------------------------------------------------------------------------
-- File       : wave_gen.vhd
-- Author     : Mohamad Ibrahim Shariat Nasseri - 256303
-- Company    : 
-- Created    : 2016-12-06
-- Last update: 2016-12-10
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Ggeneric triangular waveform generator. can be adjusted with
-- the max count and the counting steps
-------------------------------------------------------------------------------
-- Copyright (c) 2016 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-12-06  1.0      Mo      Created
-- 2016-12-06  1.0      Mo      update max/min definition to constant
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sine_wave is
  generic (
    width_g : integer := 16;                  --counter width
    num_approx  : integer := 1024                   -- Number of approximations
    );

  port (
    clk           : in  std_logic;
    rst_n         : in  std_logic;
    sync_clear_in : in  std_logic;      -- when 0, system works normally
    value_out     : out std_logic_vector (width_g-1 downto 0)
    );
end sine_wave;

architecture behavior of sine_wave is
  
  --type trigonometric_type is array (0 to num_approx-1) of integer; --std_logic_vector (width_g-1 downto 0)

  signal sin : std_logic_vector (width_g-1 downto 0) := (others => 0);
  signal cos : std_logic_vector (width_g-1 downto 0) := (others => 0);
  
  signal iter	: integer := 0;             -- counter
  constant d 	: integer := 62831/num_approx;	-- (2*pi/N) times 10,000 to make it integer
  


begin

  --system output
  value_out <= std_logic_vector (to_signed (sin(iter), width_g));

  process (clk, rst_n)
  begin
    if rst_n = '0' then
      --count   <= 0;
      --up_down <= '0';
	  
	  iter <= 0;
	  sin(0) <= 0;
	  cos(0) <= 1;
	  
    elsif clk'event and clk = '1' then

      -- checking sync_clear signal
      if sync_clear_in = '1' then
        --count   <= 0;
        --up_down <= '0';
		
		iter <= 0;
		sin(0) <= 0;
	    cos(0) <= 1;
		
	  else
		if iter /= num_approx-1 then
			sin(iter+1) <= (sin(iter)*10000) + d*cos(iter);
			cos(iter+1) <= (cos(iter)*10000) - d*sin(iter);
			iter <= iter + 1;
		else
			iter <= 0;
		end if;
		
		
		
			
       -- --counting up until max
       -- if up_down = '0' then
       --   if count = max then
       --     up_down <= '1';
       --     count   <= count - step_g;
       --   else
       --     count <= count + step_g;
       --   end if;
       --
       -- -- counting down until min     
       -- elsif up_down = '1' then
       --   if count = min then
       --     up_down <= '0';
       --     count   <= count + step_g;
       --   else
       --     count <= count - step_g;
       --   end if;
       -- end if;
      end if;
    end if;
  end process;
end;
