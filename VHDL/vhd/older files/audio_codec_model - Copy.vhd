library ieee;
use ieee.std_logic_1164.all;


entity audio_codec_model is
	generic (
			data_width_g : integer := 16
			);
			
	port 	(
			--controls
			rst_n : in std_logic;
			
			--inputs
			aud_data_in : in std_logic;
			aud_lrclk_in : in std_logic;
			aud_bclk_in : in std_logic;
			
			--outputs
			value_left_out : out std_logic_vector (data_width_g-1 downto 0);
			value_right_out : out std_logic_vector (data_width_g-1 downto 0)
			);
		
end audio_codec_model;

architecture rtl of audio_codec_model is
	
	--state type definistion
	type state_type is (wait_for_input, read_left, read_right);
	signal channel : state_type;
	
	signal output_left_en : std_logic;
	signal output_right_en : std_logic;
	
	signal left_reg : std_logic_vector (data_width_g-1 downto 0);
	signal right_reg : std_logic_vector (data_width_g-1 downto 0);
	
	--signal bclk_falling_edge
	signal lrclk_rising_edge : std_logic;
	signal lrclk_falling_edge : std_logic;
	signal lrclk_reg : std_logic;
	
	signal cnt_left : integer range 0 to data_width_g-1;
	signal cnt_right : integer range 0 to data_width_g-1;
	
	constant data_max_length_c : integer := data_width_g;
		
		
begin
	
	-- edge detections : both rising and falling edge
	lrclk_falling_edge <= lrclk_reg AND (NOT aud_lrclk_in);
	lrclk_rising_edge <= (NOT lrclk_reg) AND aud_lrclk_in;
	
	-- registering aud_lrclk_in input for edge detection purpose
	lrclk_edge: process (rst_n, aud_bclk_in)
			begin
				if rst_n = '0' then
					lrclk_reg <= '0';
				elsif aud_bclk_in'event and aud_bclk_in = '1' then
					lrclk_reg <= aud_lrclk_in;
				end if;
		end process lrclk_edge;
		
	state_trans: process (rst_n, aud_bclk_in)
			begin
				if rst_n = '0' then
					channel <= wait_for_input;
				elsif aud_bclk_in'event and aud_bclk_in = '1' then
					case channel is
						when wait_for_input =>
							if lrclk_rising_edge = '1' then
								channel <= read_left;
							elsif lrclk_falling_edge = '1' then
								channel <= read_right;
							else
								channel <= wait_for_input;
							end if;
						
						when read_left =>
							if cnt_left /= data_max_length_c then
								channel <= read_left;
							else
								if lrclk_falling_edge = '1' then								
									channel <= read_right;
								else
									channel <= wait_for_input;
								end if;
							end if;
						when read_right =>
							if cnt_right /= data_max_length_c then
								channel <= read_right;
							else
								if lrclk_rising_edge = '1' then								
									channel <= read_left;
								else
									channel <= wait_for_input;
								end if;
							end if;
					end case;
				end if;
		end process state_trans;
	
	output_logic: process (channel, lrclk_falling_edge, lrclk_rising_edge, right_reg, left_reg, aud_data_in, cnt_left, cnt_right)
			begin
				case channel is
					when wait_for_input =>
						
						if lrclk_rising_edge = '1' then
							left_reg(data_width_g-1 downto 1) <= left_reg(data_width_g-2 downto 0);
							right_reg (0) <= aud_data_in;
							cnt_left <= cnt_left + 1;
							cnt_right <= 0;
						elsif lrclk_falling_edge = '0' then
							right_reg(data_width_g-1 downto 1) <= right_reg(data_width_g-2 downto 0);
							right_reg (0) <= aud_data_in;
							cnt_right <= cnt_right + 1;
							cnt_left <= 0;
						else
							left_reg <= left_reg;
							right_reg <= right_reg;
							cnt_left <= 0;
							cnt_right <= 0;
						end if;
					
					when read_left =>
						if cnt_left /= data_max_length_c + 1 then
							left_reg(data_width_g-1 downto 1) <= left_reg(data_width_g-2 downto 0);
							left_reg (0) <= aud_data_in;
							cnt_left <= cnt_left + 1;
						else
							if lrclk_falling_edge = '1' then
							
								-- if lrclk changed, add to the other output channel
								right_reg(data_width_g-1 downto 1) <= right_reg(data_width_g-2 downto 0);
								right_reg (0) <= aud_data_in;
								cnt_right <= cnt_right + 1;
								cnt_left <= 0;
							else
								left_reg <= left_reg;
								right_reg <= right_reg;
								cnt_left <= 0;
								cnt_right <= 0;
								
							end if;
						end if;
					
					when read_right =>
						if cnt_right /= data_max_length_c + 1 then
							right_reg(data_width_g-1 downto 1) <= right_reg(data_width_g-2 downto 0);
							right_reg (0) <= aud_data_in;
							cnt_right <= cnt_right + 1;
						else
						
							if lrclk_rising_edge = '1' then
								
								-- if lrclk changed, add to the other output channel
								left_reg(data_width_g-1 downto 1) <= left_reg(data_width_g-2 downto 0);
								right_reg (0) <= aud_data_in;
								cnt_left <= cnt_left + 1;
								cnt_right <= 0;
							else
								left_reg <= left_reg;
								right_reg <= right_reg;
								cnt_left <= 0;
								cnt_right <= 0;
								
							end if;
						end if;
				end case;
		end process output_logic;
end;		