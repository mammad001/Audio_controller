-------------------------------------------------------------------------------
-- Title      : E10 - Synthesizer
-- Project    : TIE-50206 Logic Synthesis
-------------------------------------------------------------------------------
-- File       : synthesizer.vhd
-- Author     : Mohamad Ibrahim Shariat Nasseri
-- Company    : TUT
-- Last update: 2017-02-03
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2017 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2017-02-03  1.0      shariatn	Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity synthesizer is
	generic (
			clk_freq_g : integer := 18432000;  -- Hz
			sample_rate_g : integer := 48000; --Hz
			data_width_g : integer := 16 ;-- bit
			n_keys_g : integer := 4
			);
	port	(
			clk : in std_logic;
			rst_n : in std_logic;
			keys_in : in std_logic_vector (n_keys_g-1 downto 0);
			aud_bclk_out : out std_logic;
			aud_data_out : out std_logic;
			aud_lrclk_out : out std_logic
			);
end synthesizer;

architecture structural of synthesizer is

	signal wave_to_adder : std_logic_vector ((n_keys_g*data_width_g)-1 downto 0);
	signal sum_adder_audio_ctrl : std_logic_vector (data_width_g-1 downto 0);	
	
	
	component wave_gen
		generic (
			width_g : integer;                  --counter width
			step_g  : integer                   -- steps
			);
		
		port (
			clk           : in  std_logic;
			rst_n         : in  std_logic;
			sync_clear_in : in  std_logic;      -- when 0, system works normally
			value_out     : out std_logic_vector (width_g-1 downto 0)
			);
	end component wave_gen;
	
	
	component multi_port_adder
		generic (
			operand_width_g   : integer;	--Width of each input
			num_of_operands_g : integer		--Number of inputs
			);
		
		port(
			clk         : in  std_logic;
			rst_n       : in  std_logic;
			
			--input with the width equal to the total width of all operands together
			operands_in : in  std_logic_vector ((operand_width_g*num_of_operands_g)-1 downto 0);
			
			--Result of addidtion
			sum_out     : out std_logic_vector (operand_width_g-1 downto 0)
			);
	end component multi_port_adder;
	
	
	component audio_ctrl
		generic (
			ref_clk_freq_g : integer;	-- Hz
			sample_rate_g  : integer;	-- Hz
			data_width_g   : integer	-- Width in bits
			);
		
		port (
			clk           : in  std_logic;
			rst_n         : in  std_logic;
			
			left_data_in  : in  std_logic_vector (data_width_g-1 downto 0);
			right_data_in : in  std_logic_vector (data_width_g-1 downto 0);
			
			aud_bclk_out  : out std_logic;
			aud_data_out  : out std_logic;
			aud_lrclk_out : out std_logic
			);
	end component audio_ctrl;

	
	begin
					
	g_wave_gen : for index in 0 to 3 generate
		i_wave_gen : wave_gen
		generic map (
			width_g       => data_width_g,
			step_g        => 2**index
			)
		port map (
			rst_n         => rst_n,
			clk           => clk,
			sync_clear_in => keys_in (index),
			value_out     => wave_to_adder (((index+1)*data_width_g)-1 downto index*data_width_g)
			);
	end generate g_wave_gen;
	
	i_mul_0: multi_port_adder
		generic map (
					operand_width_g => data_width_g,
		            num_of_operands_g => n_keys_g
					)
		port map 	(
					clk => clk,
					rst_n => rst_n,
					operands_in => wave_to_adder,
					sum_out => sum_adder_audio_ctrl
					);
	
	i_aud_0 : audio_ctrl
		generic map (
					ref_clk_freq_g => clk_freq_g,
		            sample_rate_g => sample_rate_g,
		            data_width_g => data_width_g
					)
		port map	(
					clk => clk,
					rst_n => rst_n,
		            left_data_in => sum_adder_audio_ctrl,
		            right_data_in => sum_adder_audio_ctrl,
		            aud_bclk_out => aud_bclk_out,
		            aud_data_out => aud_data_out,
		            aud_lrclk_out => aud_lrclk_out
					);
					
end;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	