-------------------------------------------------------------------------------
-- Title      : E09 - Audio Codec Model
-- Project    : TIE-50206 Logic Synthesis
-------------------------------------------------------------------------------
-- File       : audio_codec_model.vhd
-- Author     : Mohamad Ibrahim Shariat Nasseri
-- Company    : TUT
-- Last update: 2017-02-03
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Models the Wolfson codec for testbenching
-------------------------------------------------------------------------------
-- Copyright (c) 2017 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2017-02-03  1.0      shariatn
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;


entity audio_codec_model is
  generic (
    data_width_g : integer := 16
    );

  port (
    --controls
    rst_n : in std_logic;

    --inputs
    aud_data_in  : in std_logic;
    aud_lrclk_in : in std_logic;
    aud_bclk_in  : in std_logic;

    --outputs
    value_left_out  : out std_logic_vector (data_width_g-1 downto 0);
    value_right_out : out std_logic_vector (data_width_g-1 downto 0)
    );

end audio_codec_model;

architecture rtl of audio_codec_model is

  --state type definition
  type state_type is (wait_for_input, read_left, read_right);
  signal channel : state_type;

  -- output enables. Gets enable when data is captured completely in the register
  signal l_output_en : std_logic;
  signal r_output_en : std_logic;

  -- shift register for converting serial data input to parallel output
  signal left_reg  : std_logic_vector (data_width_g-1 downto 0);
  signal right_reg : std_logic_vector (data_width_g-1 downto 0);
  
  
begin

  -- registering output - contains enable signal
  process (rst_n, aud_bclk_in)
  begin
    if rst_n = '0' then
      value_left_out  <= (others => '0');
      value_right_out <= (others => '0');
    elsif (aud_bclk_in'event and aud_bclk_in = '1') then
      if l_output_en = '1' then
        value_left_out <= left_reg;
      elsif r_output_en = '1' then
        value_right_out <= right_reg;
      end if;
    end if;
  end process;


  -- Defining state transitions

  state_trans : process (rst_n, aud_bclk_in)
  begin
    if rst_n = '0' then
      channel <= wait_for_input;
    elsif aud_bclk_in'event and aud_bclk_in = '1' then
      case channel is
        when wait_for_input =>
          if aud_lrclk_in = '1' then
            channel <= read_left;
          elsif aud_lrclk_in = '0' then
            channel <= read_right;
          else
            channel <= wait_for_input;
          end if;
          
        when read_left =>
          if aud_lrclk_in = '0' then
            channel <= read_right;
          else
            channel <= read_left;
          end if;
        when read_right =>
          if aud_lrclk_in = '1' then
            channel <= read_left;
          else
            channel <= read_right;
          end if;
      end case;
    end if;
  end process state_trans;

  -- defining outputs on each states
  output_logic : process (rst_n, aud_bclk_in)
  begin
    if rst_n = '0' then
      right_reg <= (others => '0');
      left_reg  <= (others => '0');
    elsif aud_bclk_in'event and aud_bclk_in = '1' then
      case channel is

        -- PS : wait_for_input
        when wait_for_input =>
          
          if aud_lrclk_in = '1' then
            left_reg <= (left_reg(data_width_g-2 downto 0) & aud_data_in);
          elsif aud_lrclk_in = '0' then
            right_reg <= (right_reg(data_width_g-2 downto 0) & aud_data_in);
          else
            left_reg  <= left_reg;
            right_reg <= right_reg;
          end if;

        -- PS: read_left  
        when read_left =>
          if aud_lrclk_in = '0' then
            right_reg   <= (right_reg(data_width_g-2 downto 0) & aud_data_in);
            left_reg    <= left_reg;
            l_output_en <= '1';         -- value of left reg should be sent out
            r_output_en <= '0';
          else
            left_reg    <= (left_reg(data_width_g-2 downto 0) & aud_data_in);
            right_reg   <= right_reg;
            l_output_en <= '0';
            r_output_en <= '0';
          end if;

        -- PS: read_right
        when read_right =>
          if aud_lrclk_in = '1' then
            left_reg    <= (left_reg(data_width_g-2 downto 0) & aud_data_in);
            right_reg   <= right_reg;
            r_output_en <= '1';  -- value of right reg should be sent out
            l_output_en <= '0';
          else
            right_reg   <= (right_reg(data_width_g-2 downto 0) & aud_data_in);
            left_reg    <= left_reg;
            l_output_en <= '0';
            r_output_en <= '0';
          end if;
      end case;
    end if;
  end process output_logic;
end;
