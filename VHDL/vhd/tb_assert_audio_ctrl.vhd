-------------------------------------------------------------------------------
-- Title      : E09 - Testbench top module
-- Project    : TIE-50206 Logic Synthesis
-------------------------------------------------------------------------------
-- File       : tb_audio_ctrl.vhd
-- Author     : Mohamad Ibrahim Shariat Nasseri
-- Company    : TUT
-- Last update: 2017-02-03
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Test bench for the Audio Controller
-------------------------------------------------------------------------------
-- Copyright (c) 2017 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2017-02-03  1.0      shariatn
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity tb_assert_audio_ctrl is

end tb_assert_audio_ctrl;

architecture testbench of tb_assert_audio_ctrl is

  -- clock period
  constant period_c : time := 50 ns;    -- 50 ns = 20 MHz

  constant data_width_c   : integer := 16;
  constant ref_clk_freq_c : integer := 18432000;  -- Hz
  constant sample_rate_c  : integer := 48000;     -- Hz

  signal rst_n : std_logic;
  signal clk   : std_logic := '0';

  -- Wave_Gen interface signals 
  signal l_data_wg_actrl : std_logic_vector (data_width_c-1 downto 0);
  signal r_data_wg_actrl : std_logic_vector (data_width_c-1 downto 0);
  signal sync_clear      : std_logic;

  -- Controller output signals
  signal aud_bit_clk : std_logic;
  signal aud_lr_clk  : std_logic;
  signal aud_data    : std_logic;

  -- Audio codec model output signals
  signal l_data_codec_tb : std_logic_vector (data_width_c-1 downto 0);
  signal r_data_codec_tb : std_logic_vector (data_width_c-1 downto 0);


-- component declarations
  component wave_gen
    generic (
      width_g : integer;                --counter width
      step_g  : integer                 -- steps
      );

    port (
      clk           : in  std_logic;
      rst_n         : in  std_logic;
      sync_clear_in : in  std_logic;    -- when 0, system works normally
      value_out     : out std_logic_vector (width_g-1 downto 0)
      );
  end component;

  component audio_ctrl is
    generic (
      ref_clk_freq_g : integer;         -- In Hz
      sample_rate_g  : integer;         -- In Hz
      data_width_g   : integer          -- Width in bits
      );

    port (
      clk           : in  std_logic;
      rst_n         : in  std_logic;
      left_data_in  : in  std_logic_vector (data_width_g-1 downto 0);
      right_data_in : in  std_logic_vector (data_width_g-1 downto 0);
      aud_bclk_out  : out std_logic;
      aud_data_out  : out std_logic;
      aud_lrclk_out : out std_logic
      );
  end component;

  component audio_codec_model is
    generic (
      data_width_g : integer
      );

    port (
      --controls
      rst_n : in std_logic;

      --inputs
      aud_data_in  : in std_logic;
      aud_lrclk_in : in std_logic;
      aud_bclk_in  : in std_logic;

      --outputs
      value_left_out  : out std_logic_vector (data_width_g-1 downto 0);
      value_right_out : out std_logic_vector (data_width_g-1 downto 0)
      );
  end component;


begin

  -- setting values for inputs
  rst_n <= '0',
           '1' after period_c*4;
  
  sync_clear <= '0',
                '1' after period_c*100,
                '0' after period_c*1000;
  
  clk <= not clk after period_c/2;

  left_wave_gen : wave_gen
    generic map (
      width_g => 16,
      step_g  => 10
      )

    port map (
      clk           => clk,
      rst_n         => rst_n,
      sync_clear_in => sync_clear,
      value_out     => l_data_wg_actrl
      );

  
  right_wave_gen : wave_gen
    generic map (
      width_g => 16,
      step_g  => 2
      )

    port map (
      clk           => clk,
      rst_n         => rst_n,
      sync_clear_in => sync_clear,
      value_out     => r_data_wg_actrl
      );

  controller : audio_ctrl
    generic map (
      ref_clk_freq_g => ref_clk_freq_c,
      sample_rate_g  => sample_rate_c,
      data_width_g   => data_width_c
      )

    port map (
      clk           => clk,
      rst_n         => rst_n,
      left_data_in  => l_data_wg_actrl,
      right_data_in => r_data_wg_actrl,
      aud_bclk_out  => aud_bit_clk,
      aud_data_out  => aud_data,
      aud_lrclk_out => aud_lr_clk
      );

  codec_model : audio_codec_model
    generic map (
      data_width_g => data_width_c
      )

    port map (
      rst_n           => rst_n,
      aud_data_in     => aud_data,
      aud_lrclk_in    => aud_lr_clk,
      aud_bclk_in     => aud_bit_clk,
      value_left_out  => l_data_codec_tb,
      value_right_out => r_data_codec_tb
      );

	  assert l_data_codec_tb = l_data_wg_actrl report "output data is different from what it came from wave gen.";
	  assert r_data_codec_tb = r_data_wg_actrl report "output data is different from what it came from wave gen.";
end;


