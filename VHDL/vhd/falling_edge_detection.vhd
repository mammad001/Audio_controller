-- Header ---------------------------------------------------------------------
-- Title      : E08 - Falling Edge Detector
-- Project    : TIE-50206 Logic Synthesis
-------------------------------------------------------------------------------
-- File       : falling_edge_detection.vhd
-- Author     : Mohamad Ibrahim Shariat Nasseri
-- Company    : TUT
-- Last update: 2017-02-03
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Falling edge detector to detect bclkfalling  edge
-------------------------------------------------------------------------------
-- Copyright (c) 2017 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2017-02-03  1.0      shariatn
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity falling_edge_detection is
  port (
    clk               : in  std_logic;
    rst_n             : in  std_logic;
    sig_in            : in  std_logic;
    edge_detected_out : out std_logic
    );
end falling_edge_detection;

architecture rtl of falling_edge_detection is
  
  signal sig_reg : std_logic;
  
begin
  edge_detected_out <= sig_reg and (not sig_in);  -- If falling edge, output will be '1'

  process (clk, rst_n)
  begin
    if rst_n = '0' then
      sig_reg <= '0';
    elsif clk'event and clk = '1' then
      sig_reg <= sig_in;
    end if;
  end process;
end;
