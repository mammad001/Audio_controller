-------------------------------------------------------------------------------
-- Title      : E02 - Ripple Carry Adder
-- Project    : Logic Synthesis Course (TIE-50206)
-------------------------------------------------------------------------------
-- File       : ripple_carry_adder.vhd
-- Author     : Mohamad Ibrahim Shariat Nasseri (256303)
-- Company    : 
-- Created    : 2016-11-13
-- Last update: 2016-11-13
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: This code initially is written for a 3-bit ripple-carry adder.
-- Can be expanded to any desired width
-------------------------------------------------------------------------------
-- Copyright (c) 2016 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-11-13  1.0      Mo      Created
-------------------------------------------------------------------------------

-- Libraries
library ieee;
use ieee.std_logic_1164.all;

-- Entity of the code

entity ripple_carry_adder is
  port(
    a_in  : in  std_logic_vector(2 downto 0);
    b_in  : in  std_logic_vector(2 downto 0);
    s_out : out std_logic_vector(3 downto 0)
    );
end ripple_carry_adder;

------------------------------------------------------------------------------

architecture gate of ripple_carry_adder is

  signal carry_ha         : std_logic;
  signal carry_fa         : std_logic;
  signal c, d, e, f, g, h : std_logic;


begin  -- gate

  -- Half Adder

  s_out(0) <= a_in(0) xor b_in(0);
  carry_ha <= a_in(0) and b_in(0);

  -- First Full Adder

  s_out(1) <= c xor carry_ha;
  c        <= a_in(1) xor b_in(1);
  d        <= c and carry_ha;
  e        <= a_in(1) and b_in(1);
  carry_fa <= d or e;

  -- Second Full Adder

  s_out(2) <= f xor carry_fa;
  f        <= a_in(2) xor b_in(2);
  g        <= f and carry_fa;
  h        <= a_in(2) and b_in(2);
  s_out(3) <= g or h;


end gate;
