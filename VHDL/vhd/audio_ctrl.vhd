-------------------------------------------------------------------------------
-- Title      : E08 - Audio Contoroller - Top Level block
-- Project    : TIE-50206 Logic Synthesis
-------------------------------------------------------------------------------
-- File       : audio_ctrl.vhd
-- Author     : Mohamad Ibrahim Shariat Nasseri
-- Company    : TUT
-- Created    : 2017-02-03
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: top level module of the audio controller
-------------------------------------------------------------------------------
-- Copyright (c) 2017 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2017-02-03  1.0      shariatn        
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity audio_ctrl is
  generic (
    ref_clk_freq_g : integer := 18432000;  --Hz
    sample_rate_g  : integer := 48000;     --Hz
    data_width_g   : integer := 16         --Width in bits
    );

  port (
    clk           : in  std_logic;
    rst_n         : in  std_logic;
	
    left_data_in  : in  std_logic_vector (data_width_g-1 downto 0);
    right_data_in : in  std_logic_vector (data_width_g-1 downto 0);
    
	aud_bclk_out  : out std_logic;
    aud_data_out  : out std_logic;
    aud_lrclk_out : out std_logic
    );

end audio_ctrl;

architecture rtl of audio_ctrl is

  -- -- input registers
  -- signal left_data_reg : std_logic_vector (data_width_g-1 downto 0);
  -- signal right_data_reg : std_logic_vector (data_width_g-1 downto 0);

  -- internal signals
  signal bclk             : std_logic;
  signal lrclk            : std_logic;
  signal no_delay_bclk    : std_logic;  -- bclk signal before delaying it 
  signal no_delay_lrclk   : std_logic;  -- lrclk signal before delaying it 
  signal no_delay_lrclk_n : std_logic;  -- invert of no_delay_lrclk
  signal data             : std_logic_vector (1 downto 0);
  signal edge             : std_logic;
  signal input_enable     : std_logic;


-- Clock dividers
  component clk_div
    generic (
      ref_clk_freq_g : integer := 18432000;  --Hz
      sample_rate_g  : integer := 48000;     --Hz
      data_width_g   : integer := 16         --Width in bits
      );

    port (
      clk               : in  std_logic;
      rst_n             :     std_logic;
      bclk_out          : out std_logic;
      lrclk_out         : out std_logic;
      bclk_non_delayed  : out std_logic;
      lrclk_non_delayed : out std_logic
      );
  end component;

-- Edge detection
  component falling_edge_detection is
    port (
      clk               : in  std_logic;
      rst_n             : in  std_logic;
      sig_in            : in  std_logic;
      edge_detected_out : out std_logic
      );
  end component;

-- Registering input and parallel to serial conversion
  component par_2_serial is
    generic (
      data_width_g : integer := 16      -- Width in bits
      );

    port (
      clk              : in  std_logic;
      rst_n            : in  std_logic;
      output_enable_in : in  std_logic;
      fall_edge        : in  std_logic;
      parallel_in      : in  std_logic_vector (data_width_g-1 downto 0);
      serial_out       : out std_logic
      );

  end component;


begin

-- Controller outputs to the device
  aud_bclk_out     <= bclk;
  aud_lrclk_out    <= lrclk;
  no_delay_lrclk_n <= not no_delay_lrclk;


  clk_divider : clk_div
    generic map (ref_clk_freq_g => ref_clk_freq_g,
                 sample_rate_g  => sample_rate_g,
                 data_width_g   => data_width_g
                 )
    port map (clk               => clk,
              rst_n             => rst_n,
              bclk_out          => bclk,
              lrclk_out         => lrclk,
              bclk_non_delayed  => no_delay_bclk,
              lrclk_non_delayed => no_delay_lrclk
              );

  Edge_detect : falling_edge_detection
    
    port map (clk               => clk,
              rst_n             => rst_n,
              sig_in            => no_delay_bclk,
              edge_detected_out => edge
              );

-- Capturing and converting right channel
  right_data : par_2_serial
    generic map (data_width_g => data_width_g)
    port map (clk              => clk,
              rst_n            => rst_n,
              output_enable_in => no_delay_lrclk_n,
              fall_edge        => edge,
              parallel_in      => right_data_in,
              serial_out       => data(0)
              );
			  
-- Capturing and converting left channel
  left_data : par_2_serial
    generic map (data_width_g => data_width_g)
    port map (clk              => clk,
              rst_n            => rst_n,
              output_enable_in => no_delay_lrclk,
              fall_edge        => edge,
              parallel_in      => left_data_in,
              serial_out       => data(1)
              );

-- output Mux
  process (lrclk, data)
  begin
    if lrclk = '1' then
      aud_data_out <= data(1);	-- left channel values
    else
      aud_data_out <= data(0);	-- right channel values
    end if;
  end process;
  
end;


