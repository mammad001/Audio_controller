-------------------------------------------------------------------------------
-- Title      : E04 - Multiport adder
-- Project    : 
-------------------------------------------------------------------------------
-- File       : multi_port_adder.vhd
-- Author     : Mohamad Ibrahim Shariat Nasseri (256303)
-- Company    : 
-- Created    : 2016-11-26
-- Last update: 2016-11-26
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: This will do the Multi port addition. initially for 4 inputs.
-------------------------------------------------------------------------------
-- Copyright (c) 2016 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-11-26  1.0      shariatn        Created
-------------------------------------------------------------------------------

-- Library declarations
library ieee;
use ieee.std_logic_1164.all;

entity multi_port_adder is
  generic (
    operand_width_g   : integer;	--Width of each input
    num_of_operands_g : integer	--Number of inputs
    );

  port(
    clk         : in  std_logic;
    rst_n       : in  std_logic;
	
	--input with the width equal to the total width of all operands together
    operands_in : in  std_logic_vector ((operand_width_g*num_of_operands_g)-1 downto 0);
	
	--Result of addidtion
    sum_out     : out std_logic_vector (operand_width_g-1 downto 0)
    );
end multi_port_adder;

architecture structural of multi_port_adder is

  -- component declaration (adder component)
  component adder
    generic (
      operand_width_g : integer
      );

    port(
      clk     : in  std_logic;
      rst_n   : in  std_logic;
      a_in    : in  std_logic_vector (operand_width_g-1 downto 0);
      b_in    : in  std_logic_vector (operand_width_g-1 downto 0);
      sum_out : out std_logic_vector (operand_width_g downto 0)
      );
  end component;

	--Type definition for intermidiate values
  type subtotal_type is array (0 to (num_of_operands_g/2)-1) of std_logic_vector (operand_width_g downto 0);

  signal subtotal : subtotal_type;
  signal total    : std_logic_vector (operand_width_g+1 downto 0);
  
  
begin

	--Fisrt adder/First level
  adder1 : adder
    generic map (operand_width_g => operand_width_g)
    port map (clk     => clk,
              rst_n   => rst_n,
              a_in    => operands_in ((operand_width_g*num_of_operands_g)-1 downto (operand_width_g*(num_of_operands_g-1))),
              b_in    => operands_in ((operand_width_g*(num_of_operands_g-1)-1) downto (operand_width_g*(num_of_operands_g-2))),
              sum_out => subtotal((num_of_operands_g/2)-1)
              );

	--Second adder/First level
  adder2 : adder
    generic map (operand_width_g => operand_width_g)
    port map (clk     => clk,
              rst_n   => rst_n,
              a_in    => operands_in ((operand_width_g*(num_of_operands_g-2)-1) downto (operand_width_g*(num_of_operands_g-3))),
              b_in    => operands_in ((operand_width_g*(num_of_operands_g-3)-1) downto (operand_width_g*(num_of_operands_g-4))),
              sum_out => subtotal ((num_of_operands_g/2)-2)
			  );

	--Third adder/Second level
  adder3 : adder
    generic map (operand_width_g => operand_width_g+1)		-- to be able to handle new width of operands after the first addidtion
    port map (clk     => clk,
              rst_n   => rst_n,
              a_in    => subtotal((num_of_operands_g/2)-1),
              b_in    => subtotal((num_of_operands_g/2)-2),
              sum_out => total
			  );

  sum_out <= total (operand_width_g-1 downto 0);	--Output of the system

	--to make sure number of operands is correct.
  assert num_of_operands_g = 4 report "Number of operands should be 4" severity failure;

end;




