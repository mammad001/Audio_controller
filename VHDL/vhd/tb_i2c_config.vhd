-------------------------------------------------------------------------------
-- Title      : E13 - tb_i2c_config
-- Project    : TIE-50206 - Logic Synthesis
-------------------------------------------------------------------------------
-- File       : tb_i2c_config.vhd
-- Author     : Mohamad Ibrahim Shariat Nasseri - 256303
-- Company    : 
-- Last update: 2017-02-26
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Testbench for i2c_config code
-------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------
-- Empty entity
-------------------------------------------------------------------------------

entity tb_i2c_config is
end tb_i2c_config;

-------------------------------------------------------------------------------
-- Architecture
-------------------------------------------------------------------------------
architecture testbench of tb_i2c_config is

  -- Number of parameters to expect
  constant n_params_c     : integer := 10;
  constant i2c_freq_c     : integer := 20000;
  constant ref_freq_c     : integer := 50000000;
  constant clock_period_c : time    := 20 ns;

  -- Every transmission consists several bytes and every byte contains given
  -- amount of bits. 
  constant n_bytes_c       : integer                       := 3;
  constant bit_count_max_c : integer                       := 8;
  constant codec_address_c : std_logic_vector (6 downto 0) := "0011010";  -- 7 bit address on i2c bus

  -- Signals fed to the DUV
  signal clk   : std_logic := '0';  -- Remember that default values supported
  signal rst_n : std_logic := '0';      -- only in synthesis

  -- The DUV prototype
  component i2c_config
    generic (
      ref_clk_freq_g : integer;
      i2c_freq_g     : integer;
      n_params_g     : integer);
    port (
      clk              : in    std_logic;
      rst_n            : in    std_logic;
      sdat_inout       : inout std_logic;
      sclk_out         : out   std_logic;
      param_status_out : out   std_logic_vector(n_params_g-1 downto 0);
      finished_out     : out   std_logic;
      dbg_mem_data_out : out   std_logic_vector (7 downto 0)
      );
  end component;

  -- Signals coming from the DUV
  signal sdat         : std_logic := 'Z';
  signal sclk         : std_logic;
  signal param_status : std_logic_vector(n_params_c-1 downto 0);
  signal finished     : std_logic;
  signal dbg_mem_data : std_logic_vector(7 downto 0);

  -- To hold the value that will be driven to sdat when sclk is high.
  signal sdat_r : std_logic;            --Used for ACK/NACK response

  -- Counters for receiving bits and bytes
  signal bit_counter_r   : integer range 0 to bit_count_max_c-1;
  signal byte_counter_r  : integer range 0 to n_bytes_c-1;
  signal param_counter_r : integer range 0 to n_params_c-1;  -- Counts number of received parameters

  signal ack_gen_counter : integer range 0 to 4;  -- Used to generate ACK/NACK signal

  -- States for the FSM
  type states is (wait_start, read_byte, send_ack, wait_stop);
  signal curr_state_r : states;

  -- Memory
  type memory_array is array (0 to n_params_c*3 -1) of std_logic_vector (7 downto 0);
  signal tb_memory : memory_array := (  -- format: Address and R/W byte, Reg. Address byte, Data byte
    "00110100", "00000000", "00011010",  -- Left Line In
    "00110100", "00000010", "00011010",  -- Right Line In
    "00110100", "00000100", "01111011",  -- Left Headphone Out
    "00110100", "00000110", "01111011",  -- Right Headphone Out
    "00110100", "00001000", "11111000",  -- Analogue Audio Path Control
    "00110100", "00001010", "00000110",  -- Digital Audio Path Control
    "00110100", "00001100", "00000000",  -- Power Down Control
    "00110100", "00001110", "00000001",  -- Digital Audio Interface Format
    "00110100", "00010000", "00000010",  -- Sampling Control
    "00110100", "00010010", "00000001"  -- Active Control
    );

  -- Previous values of the I2C signals for edge detection
  signal sdat_old_r : std_logic;
  signal sclk_old_r : std_logic;
  
begin  -- testbench

  clk   <= not clk after clock_period_c/2;
  rst_n <= '1'     after clock_period_c*4;

  -- Assign sdat_r when sclk is active, otherwise 'Z'.
  -- Note that sdat_r is usually 'Z'
  with sclk select sdat <=
    sdat_r when '1',
    'Z'    when others;


  -- Component instantiation
  i2c_config_1 : i2c_config
    generic map (
      ref_clk_freq_g => ref_freq_c,
      i2c_freq_g     => i2c_freq_c,
      n_params_g     => n_params_c)
    port map (
      clk              => clk,
      rst_n            => rst_n,
      sdat_inout       => sdat,
      sclk_out         => sclk,
      param_status_out => param_status,
      finished_out     => finished,
      dbg_mem_data_out => dbg_mem_data);
  -----------------------------------------------------------------------------
  -- The main process that controls the behavior of the test bench
  fsm_proc : process (clk, rst_n)
  begin  -- process fsm_proc
    if rst_n = '0' then                 -- asynchronous reset (active low)

      curr_state_r <= wait_start;

      sdat_old_r <= '0';
      sclk_old_r <= '0';

      byte_counter_r <= 0;
      bit_counter_r  <= 0;

      sdat_r <= 'Z';
      
    elsif clk'event and clk = '1' then  -- rising clock edge

      -- The previous values are required for the edge detection
      sclk_old_r <= sclk;
      sdat_old_r <= sdat;


      -- Falling edge detection for acknowledge control
      -- Must be done on the falling edge in order to be stable during
      -- the high period of sclk
      if sclk = '0' and sclk_old_r = '1' then

        -- If we are supposed to send ack
        if curr_state_r = send_ack then

          -- Send ack (low = ACK, high = NACK)
          if ack_gen_counter /= 4 then
            sdat_r          <= '0';     --ACK
            ack_gen_counter <= ack_gen_counter+1;
          else
            sdat_r          <= '1';     --NACK
            ack_gen_counter <= 0;
          end if;

        else

          -- Otherwise, sdat is in high impedance state.
          sdat_r <= 'Z';
          
        end if;
        
      end if;


      -------------------------------------------------------------------------
      -- FSM
      case curr_state_r is

        -----------------------------------------------------------------------
        -- Wait for the start condition
        when wait_start =>

          -- While clk stays high, the sdat falls
          if sclk = '1' and sclk_old_r = '1' and
            sdat_old_r = '1' and sdat = '0' then

            curr_state_r <= read_byte;

          end if;

        --------------------------------------------------------------------
        -- Wait for a byte to be read
        when read_byte =>

          -- Detect a rising edge
          if sclk = '1' and sclk_old_r = '0' then

            if bit_counter_r /= bit_count_max_c-1 then
              
              if byte_counter_r /= 0 then  -- Register address byte and data byte

                -- To check if memory values in i2c_config are as specified in requirement
                assert dbg_mem_data = tb_memory((param_counter_r*3)+byte_counter_r)
                  report "Byte '" & integer'image(byte_counter_r) & "' of parameter '"
					& integer'image(param_counter_r) & "' should be same as what is specified in requirement."
                  severity failure;

                -- Assert to check values are received intact
				-- Index is "bit_count_max_c - bit_counter_r -1" because data comes with MSB first
                assert sdat = dbg_mem_data(bit_count_max_c - bit_counter_r -1)
                  report "Byte """ & integer'image(byte_counter_r) &
					""" of parameter """ & integer'image(param_counter_r) &
					""" is different from what it was sent"
                  severity failure;
				  
              else                      -- Device address byte
                -- Assert for checking the address
                assert sdat = codec_address_c(bit_count_max_c-bit_counter_r-2)  
                  report "Device Address is wrong. it should be ""0011010""."
                  severity failure;
              end if;

              -- Normally just receive a bit
              bit_counter_r <= bit_counter_r + 1;

            else
              if byte_counter_r = 0 then
                
				-- Checks R/W' bit when receiving address byte.
                -- Slave device only accepts write commands
                assert sdat = '0'
                  report "Codec device does not support read mode"
                  severity failure;
				  
              end if;

              -- When terminal count is reached, let's send the ack
              curr_state_r  <= send_ack;
              bit_counter_r <= 0;
              
            end if;  -- Bit counter terminal count
            
          end if;  -- sclk rising clock edge

        --------------------------------------------------------------------
        -- Send acknowledge
        when send_ack =>

          -- Detect a rising edge
          if sclk = '1' and sclk_old_r = '0' then
            if sdat_r = '0' then
              if byte_counter_r /= n_bytes_c-1 then

                -- Transmission continues
                byte_counter_r <= byte_counter_r + 1;
                curr_state_r   <= read_byte;
                
              else

                -- Transmission is about to stop
                if param_counter_r /= n_params_c-1 then  -- There's still parameters to send 
                  byte_counter_r  <= 0;
                  curr_state_r    <= wait_start;
                  param_counter_r <= param_counter_r+1;
                else                    -- All parameters are sent successfully
                  byte_counter_r <= 0;
                  curr_state_r   <= wait_stop;
                end if;
                
              end if;
            else
              curr_state_r <= wait_start;  -- If NACK, start again.
			  byte_counter_r <= 0;
            end if;

          end if;

        ---------------------------------------------------------------------
        -- Wait for the stop condition
        when wait_stop =>

          -- Stop condition detection: sdat rises while sclk stays high
          if sclk = '1' and sclk_old_r = '1' and
            sdat_old_r = '0' and sdat = '1' then

            curr_state_r <= wait_start;
            
          end if;

      end case;

    end if;
  end process fsm_proc;

  -----------------------------------------------------------------------------
  -- Asserts for verification
  -----------------------------------------------------------------------------

  -- SDAT should never contain X:s.
  assert sdat /= 'X' report "Three state bus in state X" severity error;

  -- End of simulation, but not during the reset
  assert finished = '0' or rst_n = '0' report
    "Simulation done" severity failure;
  
end testbench;
