-- Header ---------------------------------------------------------------------
-- Title      : E08 - Clock Divider
-- Project    : TIE-50206 Logic Synthesis
-------------------------------------------------------------------------------
-- File       : clk_div.vhd
-- Author     : Mohamad Ibrahim Shariat Nasseri
-- Company    : TUT
-- Last Update: 2017-02-03
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: For dividing the clock to be used for LRCLK & BCLK signals
-------------------------------------------------------------------------------
-- Copyright (c) 2017 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2017-02-03  1.0      shariatn
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity clk_div is
  generic (
    ref_clk_freq_g : integer := 18432000;  --Hz
    sample_rate_g  : integer := 48000;     --Hz
    data_width_g   : integer := 16         --Width in bits
    );
  port (
    clk   : in std_logic;
    rst_n :    std_logic;

    -- registered clock outputs
    bclk_out  : out std_logic;
    lrclk_out : out std_logic;

    -- unregistered clock outputs for starting operations soon enough
    bclk_non_delayed  : out std_logic;
    lrclk_non_delayed : out std_logic
    );
end clk_div;


architecture behavior of clk_div is

-- internal signals
  signal bclk  : std_logic;
  signal lrclk : std_logic;

-- for registering the outputs
  signal bclk_reg  : std_logic;
  signal lrclk_reg : std_logic;

-- clock divisions counters
  signal bclk_cntr  : integer;
  signal lrclk_cntr : integer;

-- max values for clk-div counters
  constant bclk_max_c  : integer := (ref_clk_freq_g/((4*(data_width_g)*sample_rate_g)))-1;
  constant lrclk_max_c : integer := (ref_clk_freq_g/(2*sample_rate_g))-1;

  
begin

  -- block outputs
  bclk_out          <= bclk_reg;
  lrclk_out         <= lrclk_reg;
  bclk_non_delayed  <= bclk;
  lrclk_non_delayed <= lrclk;

  --for Registering and delaying output
  delay_proc : process (clk, rst_n)
  begin
    if rst_n = '0' then
      bclk_reg  <= '1';
      lrclk_reg <= '0';
    elsif clk'event and clk = '1' then
      bclk_reg  <= bclk;
      lrclk_reg <= lrclk;
    end if;
  end process delay_proc;

  -- Creating LRCLK signal
  lrclk_proc : process (clk, rst_n)
  begin
    if rst_n = '0' then
      lrclk      <= '0';
      lrclk_cntr <= lrclk_max_c;
    elsif clk'event and clk = '1' then
      if lrclk_cntr = lrclk_max_c then
        lrclk      <= not lrclk;
        lrclk_cntr <= 0;
      else
        lrclk_cntr <= lrclk_cntr + 1;
        lrclk      <= lrclk;
      end if;
    end if;
  end process lrclk_proc;

  -- Creating BCLK signal
  bclk_proc : process (clk, rst_n)
  begin
    if rst_n = '0' then
      bclk      <= '1';
      bclk_cntr <= bclk_max_c;
    elsif clk'event and clk = '1' then
      if bclk_cntr = bclk_max_c then
        bclk      <= not bclk;
        bclk_cntr <= 0;
      else
        bclk      <= bclk;
        bclk_cntr <= bclk_cntr + 1;
      end if;
    end if;
  end process bclk_proc;
end;
