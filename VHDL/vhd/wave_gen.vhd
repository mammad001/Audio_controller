-------------------------------------------------------------------------------
-- Title      : E06 - Triangular waveform generator
-- Project    : TIE-50206 - Logic Synthesis
-------------------------------------------------------------------------------
-- File       : wave_gen.vhd
-- Author     : Mohamad Ibrahim Shariat Nasseri - 256303
-- Company    : 
-- Created    : 2016-12-06
-- Last update: 2016-12-10
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Ggeneric triangular waveform generator. can be adjusted with
-- the max count and the counting steps
-------------------------------------------------------------------------------
-- Copyright (c) 2016 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-12-06  1.0      Mo      Created
-- 2016-12-06  1.0      Mo      update max/min definition to constant
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity wave_gen is
  generic (
    width_g : integer;                  --counter width
    step_g  : integer                   -- steps
    );

  port (
    clk           : in  std_logic;
    rst_n         : in  std_logic;
    sync_clear_in : in  std_logic;      -- when 0, system works normally
    value_out     : out std_logic_vector (width_g-1 downto 0)
    );
end wave_gen;

architecture behavior of wave_gen is

  constant max	: integer := (((2**(width_g-2))-1)/step_g)*step_g;             -- maximum value
  constant min	: integer := -(max);             -- Minimum Value
  signal count   : integer;
  signal up_down : std_logic;  -- when 0, counts up, when 1, counts down

begin

  --system output
  value_out <= std_logic_vector (to_signed (count, width_g));

  process (clk, rst_n)
  begin
    if rst_n = '0' then
      count   <= 0;
      up_down <= '0';
    elsif clk'event and clk = '1' then

      -- checking sync_clear signal
      if sync_clear_in = '1' then
        count   <= 0;
        up_down <= '0';
      elsif sync_clear_in = '0' then

        --counting up until max
        if up_down = '0' then
          if count = max then
            up_down <= '1';
            count   <= count - step_g;
          else
            count <= count + step_g;
          end if;

        -- counting down until min     
        elsif up_down = '1' then
          if count = min then
            up_down <= '0';
            count   <= count + step_g;
          else
            count <= count - step_g;
          end if;
        end if;
      end if;
    end if;
  end process;
end;
