-------------------------------------------------------------------------------
-- Title      : E12 - i2c_config
-- Project    : TIE-50206 - Logic Synthesis
-------------------------------------------------------------------------------
-- File       : i2c_config.vhd
-- Author     : Mohamad Ibrahim Shariat NAsseri - 256303
-- Company    : 
-- Last update: 2017-02-26
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: code for i2c block of the audio synthesizer

-------------------------------------------------------------------------------



library ieee;
use ieee.std_logic_1164.all;

entity i2c_config is
  generic (
    ref_clk_freq_g : integer := 50000000;  --Hz
    i2c_freq_g     : integer := 20000;  --Hz
    n_params_g     : integer := 10      --number of configuration transmission
    );

  port (
    clk              : in    std_logic;
    rst_n            : in    std_logic;
    sdat_inout       : inout std_logic;
    sclk_out         : out   std_logic;
    param_status_out : out   std_logic_vector(n_params_g-1 downto 0);
    finished_out     : out   std_logic;
    dbg_mem_data_out : out   std_logic_vector (7 downto 0)
    );

end i2c_config;

architecture rtl of i2c_config is
  
  -- Type definistion for FSM and memory
  type state_type is (initial,          -- Initial state after reset
                      start_transmit,   -- For starting transmission
                      byte_transmit,    -- Sends data to slave
                      ack_response,     -- check slave response (ACK/NACK)
                      restart_transmit,  -- Starts transmission. only is used after getting NACK
                      stop_transmit);  -- Final state. Terminates the transmission

  type memory_array is array (0 to n_params_g*3 -1) of std_logic_vector (7 downto 0);
  
    -- states declarations
    signal cur_state : state_type;
  signal next_state : state_type;
  
  -- Memory and the values based on Exercise 12
  -- format: address and R/W, 1st data byte, 2nd data byte
  signal memory : memory_array := (  
      "00110100", "00000000", "00011010",  -- Left Line In
      "00110100", "00000010", "00011010",  -- Right Line In
      "00110100", "00000100", "01111011",  -- Left Headphone Out
      "00110100", "00000110", "01111011",  -- Right Headphone Out
      "00110100", "00001000", "11111000",  -- Analogue Audio Path Control
      "00110100", "00001010", "00000110",  -- Digital Audio Path Control
      "00110100", "00001100", "00000000",  -- Power Down Control
      "00110100", "00001110", "00000001",  -- Digital Audio Interface Format
      "00110100", "00010000", "00000010",  -- Sampling Control
      "00110100", "00010010", "00000001"   -- Active Control
      );

  -- Clock division Signals and constants
  constant max_count : integer := (ref_clk_freq_g/(2*i2c_freq_g));
  
  signal clk_counter : integer := max_count - 1; 
  signal clk_div : std_logic;         -- Clock division output signal
  signal half_duty    : std_logic;  -- Will be '1' when clk_counter
                                    --reaches its half point. Otherwise, will be '0'
  signal quarter_duty : std_logic;  -- Will be '1' when clk_counter
                                    --reaches 1/4 of its max value. Otherwise, will be '0'
  
  
  -- keeps the value that is being transmitted at the moment
  signal byte_handler : std_logic_vector (7 downto 0);  
  
  -- Counters
  signal bit_counter : integer;  -- Counts number of sent bits in the current byte
  signal mem_ptr : integer := 0;  -- Index of the byte in the memory that is being sent
  signal ack     : integer := 0;  -- Will count if there's NACK. Otherwise will be '0'


  --State transition flags
  signal initialized   : std_logic;     -- if '1', initialization is done
  signal start_done    : std_logic;  -- if '1', transmission is started successfully 
  signal byte_done     : std_logic;  -- if '1', one byte is sent to the slave
  signal ack_received  : std_logic;  -- if '1', sent byte is received successfully. Otherwise there's NACK
  signal transfer_done : std_logic;  -- if '1', All bytes are sent successfully and its related ACK is received
  signal wait_for_ack  : std_logic;  -- if '1', Waiting for the response from slave
  signal restart_done  : std_logic;  -- if '1', transmission is restarted successfully
  
  
begin
  
  -- Connecting clock division output to the block output
  sclk_out <= clk_div;
  
    -- Creating SCLK signal
    clk_division : process (clk, rst_n)

    begin
      if rst_n = '0' then
        clk_counter  <= 0;
        clk_div      <= '0';
        half_duty    <= '0';
        quarter_duty <= '0';
      elsif clk'event and clk = '1' then
        if clk_counter = max_count-1 then
          clk_div      <= not clk_div;
          clk_counter  <= 0;
          half_duty    <= '0';
          quarter_duty <= '0';
        elsif clk_counter = (max_count/2)-1 then   -- when half_duty= '1'
          clk_div      <= clk_div;
          clk_counter  <= clk_counter + 1;
          half_duty    <= '1';
          quarter_duty <= '0';
        elsif clk_counter = (max_count/4)-1 then   -- when quarter_duty= '1'
          clk_div      <= clk_div;
          clk_counter  <= clk_counter + 1;
          half_duty    <= '0';
          quarter_duty <= '1';
        else
          clk_div      <= clk_div;
          clk_counter  <= clk_counter + 1;
          half_duty    <= '0';
          quarter_duty <= '0';
        end if;
      end if;
    end process clk_division;
    
      
      -- Moving next state to the current one
      state_trans : process (clk, rst_n)
      begin
        if rst_n = '0' then
          cur_state <= initial;
        elsif clk'event and clk = '1' then
          cur_state <= next_state;
        end if;
      end process state_trans;
      
        -- Logic to select the next state
        next_state_logic : process (cur_state, initialized, start_done, byte_done,
									ack_received, transfer_done, wait_for_ack,
									restart_done, mem_ptr)
        begin
          case cur_state is
            when initial =>
              if initialized = '1' then
                next_state <= start_transmit;
              else
                next_state <= initial;
              end if;
            when start_transmit =>
              if start_done = '1' then
                next_state <= byte_transmit;
              else
                next_state <= start_transmit;
              end if;
            when byte_transmit =>
              if byte_done = '1' then
                next_state <= ack_response;
              else
                next_state <= byte_transmit;
              end if;
            when ack_response =>
              if ack_received = '1' then
                if transfer_done = '1' then
                  next_state <= stop_transmit;
                else
                  if (mem_ptr) mod 3 = 0 then  -- one complete transmission for one parameter done.
												--start transmission for the next parameter
                    next_state <= start_transmit;
                  else  -- go to the next byte whitin the transmission
                    next_state <= byte_transmit;
                  end if;
                end if;
              else
                if wait_for_ack = '1' then
                  next_state <= ack_response;  -- waiting to get response
                else
                  next_state <= restart_transmit;  -- NACK received. Restart the transmission
                end if;
              end if;
            when restart_transmit =>
              if restart_done = '1' then
                next_state <= byte_transmit;
              else
                next_state <= restart_transmit;
              end if;
            when stop_transmit =>  -- Transfer done succefully. Device ready to use
              next_state <= stop_transmit;
          end case;
        end process next_state_logic;
        
          -- State outputs      
          state_output : process (clk, rst_n)
          begin
            if rst_n = '0' then
              byte_handler     <= (others => '0');
              bit_counter      <= 0;
              sdat_inout       <= '1';
              param_status_out <= (others => '0');
              finished_out     <= '0';
              ack_received     <= '0';
              wait_for_ack     <= '1';
              ack              <= 0;
              dbg_mem_data_out <= (others => '0');
              mem_ptr          <= 0;
              byte_handler     <= (others => '0');
              bit_counter      <= 0;
              
            elsif clk'event and clk = '1' then
              case cur_state is
                when initial =>
                  if clk_div = '0' and quarter_duty = '1' then
                    sdat_inout  <= '1';
                    initialized <= '1';
                  else
                    initialized <= '0';
                  end if;
                when start_transmit =>
                  if clk_div = '1' and half_duty = '1' then  -- pull down the sdat_inout to start
                    sdat_inout <= '0';
                    start_done <= '0';
                  elsif clk_div = '0' and quarter_duty = '1' then  --Start already done. move to the next state
                    start_done       <= '1';
                    byte_handler     <= memory(mem_ptr);
                    dbg_mem_data_out <= memory(mem_ptr);  -- for debugging use
                  else
                    sdat_inout <= sdat_inout;
                    start_done <= '0';
                  end if;
                  
                when byte_transmit =>
                  if clk_div = '0' and half_duty = '1' then  -- Send out MSB of the remaining bits
                    sdat_inout   <= byte_handler(7);
                    bit_counter  <= bit_counter + 1;
                    byte_handler <= byte_handler(6 downto 0) & '0';
                    byte_done    <= '0';
                  elsif clk_div = '0' and quarter_duty = '1' then
                    if bit_counter = 8 then                  -- one byte sent.
                      byte_done <= '1';
                    else
                      byte_done <= '0';
                    end if;
                  end if;
                  
                when ack_response =>
                  bit_counter <= 0;
                  byte_done   <= '0';
                  if clk_div = '0' and half_duty = '1' then
                    sdat_inout <= 'Z';  -- release the channel
                  elsif clk_div = '0' and quarter_duty = '1' then
                    if ack = 0 then     -- ACK received
                      ack_received <= '1';
                      wait_for_ack <= '0';

                                        -- load the next byte
                      if mem_ptr = (n_params_g*3 -1) then  -- all bytes are already sent
                        transfer_done <= '1';
                        sdat_inout    <= '0';  -- Pull down sdat_inout before going to stop_transmit
                      elsif (mem_ptr+1) mod 3 = 0 then  -- one parameter is done. go to the next parameter
                        byte_handler                    <= memory(mem_ptr+1);
                        dbg_mem_data_out                <= memory(mem_ptr+1);  -- for debugging use
                        transfer_done                   <= '0';
                        mem_ptr                         <= mem_ptr + 1;
                        sdat_inout                      <= '1';  -- Pull up sdat_inout before going to start_transmit.
                        param_status_out((mem_ptr-2)/3) <= '1';  -- Turn on LED of that parameter
                      else
                        byte_handler     <= memory(mem_ptr+1);
                        dbg_mem_data_out <= memory(mem_ptr+1);  -- for debugging use
                        transfer_done    <= '0';
                        mem_ptr          <= mem_ptr + 1;
                      end if;
                      
                    else
                      sdat_inout   <= '1';  -- NACK received. Will restart the transmission
                      ack_received <= '0';
                      wait_for_ack <= '0';
                      ack          <= 0;
					  mem_ptr          <= (mem_ptr/3)*3;  -- Return to base address of the current parameter
					  -- byte_handler     <= memory(mem_ptr);
					  -- dbg_mem_data_out <= memory(mem_ptr);  -- for debugging use
					  
                    end if;
                  elsif clk_div = '1' then
                    wait_for_ack <= '1';
                    if sdat_inout = '1' then  -- If NACK, ack++
                      ack <= ack + 1;
                    else
                      ack <= ack;
                    end if;
                  else
                    ack_received <= '0';
                    wait_for_ack <= '1';
                  end if;
                  
                when restart_transmit =>
                  if clk_div = '1' and half_duty = '1' then
                    sdat_inout   <= '0';
                    restart_done <= '0';
                  elsif clk_div = '0' and quarter_duty = '1' then
                    restart_done     <= '1';
                    -- mem_ptr          <= (mem_ptr/3)*3;  -- Return to base address of the current parameter
                    byte_handler     <= memory(mem_ptr);
                    dbg_mem_data_out <= memory(mem_ptr);  -- for debugging use
                  else
                    sdat_inout   <= sdat_inout;
                    restart_done <= '0';
                  end if;
                  
                when stop_transmit =>
                  if clk_div = '1' and half_duty = '1' then
                    sdat_inout                      <= '1';  -- pulling up sdat_inout to stop the transmission
                    finished_out                    <= '1';  -- Transmission done. LED turned on
                    param_status_out (n_params_g-1) <= '1';
                    

                  end if;
              end case;
            end if;
          end process state_output;
          end;
