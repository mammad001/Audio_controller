-------------------------------------------------------------------------------
-- Title      : E08 - Parallel to Serial Converter
-- Project    : TIE-50206 Logic Synthesis
-------------------------------------------------------------------------------
-- File       : par_2_serial.vhd
-- Author     : MOhamad Ibrahim Shariat Nasseri
-- Company    : TUT
-- Last Update: 2017-02-03
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Registers inputs & does the parallel to serial conversion
-------------------------------------------------------------------------------
-- Copyright (c) 2017 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2017-02-03  1.0      shariatn
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity par_2_serial is
  generic (
    data_width_g : integer := 16        -- Width in bits
    );

  port (
    clk              : in  std_logic;
    rst_n            : in  std_logic;
    output_enable_in : in  std_logic;
    fall_edge        : in  std_logic;
    parallel_in      : in  std_logic_vector (data_width_g-1 downto 0);
    serial_out       : out std_logic
    );

end par_2_serial;

architecture rtl of par_2_serial is

  constant maxcount_c : integer := data_width_g-1;  --Max valur of the counter

  signal shift_cnt : integer;           -- counter to keep track of shifting

  signal serial    : std_logic;
  signal data_reg  : std_logic_vector (data_width_g-1 downto 0);  -- Registered input value
  signal shift_in  : std_logic_vector (data_width_g-1 downto 0);  -- shift register input
  signal shift_out : std_logic_vector (data_width_g-1 downto 0);  -- shift register output


  
begin

  -- block_output       
  serial_out <= serial;

  -- Registering the input              
  process (clk, rst_n)
  begin
    if rst_n = '0' then
      data_reg <= (others => '0');
    elsif clk'event and clk = '1' then
      data_reg <= parallel_in;
    end if;
  end process;


  -- Mux for shifter input
  -- If it's this block's turn to send the output, load the previous value of shifter
  -- If it's not, keep loading value from input
  process (output_enable_in, data_reg, shift_out)
  begin
    if output_enable_in = '1' then
      shift_in <= shift_out;
    else
      shift_in <= data_reg;
    end if;
  end process;


  -- parallel to serial conversion              
  par_to_ser : process (clk, rst_n)
  begin
    if rst_n = '0' then

      -- Initialization
      serial    <= '0';
      shift_out <= (others => '0');
      shift_cnt <= 0;
      
    elsif clk'event and clk = '1' then

      -- if LRCLK is pointing to this channel, and there's falling edge of BCLK do this
      if output_enable_in = '1' and fall_edge = '1' then
        if shift_cnt = 0 then
          serial                              <= shift_in (data_width_g-1);
          shift_out (data_width_g-1 downto 1) <= shift_in (data_width_g-2 downto 0);
          shift_cnt                           <= shift_cnt + 1;
        elsif shift_cnt = maxcount_c then
          serial <= shift_out (data_width_g-1);
        else
          serial                              <= shift_out (data_width_g-1);
          shift_out (data_width_g-1 downto 1) <= shift_in (data_width_g-2 downto 0);
          shift_cnt                           <= shift_cnt + 1;
        end if;

      -- if it's not the turn of this channel, do this
      elsif output_enable_in = '0' then
        shift_cnt <= 0;
        serial    <= serial;
        shift_out <= shift_in;

      -- if none of above, then keep the last value on "serial" signal
      else
        serial <= serial;
      end if;
    end if;
  end process par_to_ser;
end;
