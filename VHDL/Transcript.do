# A bookmarks file was found in the current directory with 1 bookmark
# //  ModelSim SE 10.2c Jul 18 2013 
# //
# //  Copyright 1991-2013 Mentor Graphics Corporation
# //  All Rights Reserved.
# //
# //  THIS WORK CONTAINS TRADE SECRET AND PROPRIETARY INFORMATION
# //  WHICH IS THE PROPERTY OF MENTOR GRAPHICS CORPORATION OR ITS
# //  LICENSORS AND IS SUBJECT TO LICENSE TERMS.
# //
# vsim -L dls_16 -l transcript.txt -i -multisource_delay latest -t ns +typdelays -foreign {hdsInit C:/Apps/MentorGraphics/HDS_2012.2a/resources/downstream/modelsim/ModelSim_32Bit.dll} -pli {"C:/Apps/MentorGraphics/HDS_2012.2a/resources/downstream/modelsim/ModelSim_32Bit.dll"} dls_16.elevator_fpga(struct) 
# ** Note: (vsim-3813) Design is being optimized due to module recompilation...
# 
# ** Note: (vsim-3865) Due to PLI being present, full design access is being specified.
# 
# Loading C:/Apps/MentorGraphics/HDS_2012.2a/resources/downstream/modelsim/ModelSim_32Bit.dll
# Loading std.standard
# Loading std.textio(body)
# Loading ieee.std_logic_1164(body)
# Loading ieee.std_logic_arith(body)
# Loading ieee.std_logic_unsigned(body)
# Loading ieee.numeric_std(body)
# Loading work.elevator_fpga(struct)#1
# Loading work.pulse_led(behavioral)#1
# A bookmarks file was found in the current directory with 1 bookmark
add wave -position end  sim:/elevator_fpga/LEDG
add wave -position insertpoint  \
sim:/elevator_fpga/en_out \
sim:/elevator_fpga/en_out1 \
sim:/elevator_fpga/en_out2 \
sim:/elevator_fpga/en_out3 \
sim:/elevator_fpga/en_out4 \
sim:/elevator_fpga/en_out5 \
sim:/elevator_fpga/en_out6 \
sim:/elevator_fpga/en_out7
add wave -position insertpoint  \
sim:/elevator_fpga/led1 \
sim:/elevator_fpga/led2 \
sim:/elevator_fpga/led3 \
sim:/elevator_fpga/led4 \
sim:/elevator_fpga/led5 \
sim:/elevator_fpga/led6 \
sim:/elevator_fpga/led7
force -freeze sim:/elevator_fpga/CLOCK_50 1 0, 0 {10 ns} -r 20
force -freeze sim:/elevator_fpga/SW 18'h00000 0
