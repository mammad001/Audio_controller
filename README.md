A simple audio controller/synthesizer for Wolfson WM8731/L on Altera DE2 board.

It includes a wave generator, audio codec controller and an I2C bus controller.